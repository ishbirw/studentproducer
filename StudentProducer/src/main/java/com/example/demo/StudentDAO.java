package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class StudentDAO {
	private List<Student> studentList;
	public StudentDAO() {
		
		studentList = new ArrayList<Student>();
		studentList.add(new Student("Ishbir","21","Delhi"));
		studentList.add(new Student("Karan","22","Delhi"));
		studentList.add(new Student("Samson","22","Mumbai"));
	}
	
	
	public List<Student> getStudentList() {
		return studentList;
	}
	public void setStudentList(List<Student> studentList) {
		this.studentList = studentList;
	}
	
	public void insertStudent(Student student)
	{
		studentList.add(student);
	}
}
