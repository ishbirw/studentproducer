package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class Controller {
	
	@Autowired
	StudentDAO studentDao;
	
	@GetMapping("/getStudents")
	public List<Student> display()
	{
		return studentDao.getStudentList();
	}
	
	@PostMapping("/addStudents")
	public void insert(@RequestBody Student student)
	{
		studentDao.insertStudent(student);
	}
}
